package niga.cristina.lab8.ex4;

public class HeatingUniy {
    public static void turnOn(int temp, int threshold){

        System.out.println("Heating unit is turned on.");
        while(temp<threshold)
        {
            System.out.println("Current temperature:"+temp);
            temp++;
        }
        System.out.println("Temperature threshold reached. Heating unit is turning off.");
    }
}
