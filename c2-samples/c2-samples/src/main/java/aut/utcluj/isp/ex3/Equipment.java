package aut.utcluj.isp.ex3;

import aut.utcluj.isp.ex4.EquipmentNotProvidedException;

import java.util.ArrayList;

/**
 * @author stefan
 */
public class Equipment {
    private String name;
    private String serialNumber;
    private String owner;
    private boolean taken;


    public Equipment(String serialNumber) {
       // throw new UnsupportedOperationException("Not supported yet.");
        this.serialNumber=serialNumber;
    }

    public Equipment(String name, String serialNumber) {
       // throw new UnsupportedOperationException("Not supported yet.");
        this.name=name;
        this.serialNumber=serialNumber;
    }

    public Equipment(String name, String serialNumber, String owner) {
      //  throw new UnsupportedOperationException("Not supported yet.");
        this.name=name;
        this.serialNumber=serialNumber;
        this.owner=owner;
    }

    public String getName() {
        return name;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public String getOwner() {
        return owner;
    }

    public boolean isTaken() {
        return taken;
    }

    /**
     * Provide the owner of the equipment
     * Equipment should be set as taken
     *
     * @param owner - owner name
     */
    public void provideEquipmentToUser(final String owner) {
       // throw new UnsupportedOperationException("Not supported yet.");
        this.owner=owner;
        taken=true;


    }

    /**
     * Equipment is returned to the office
     * Equipment should not be set as taken
     * Remove the owner
     */
    public void returnEquipmentToOffice() {

      //  throw new UnsupportedOperationException("Not supported yet.");
        this.owner=null ;
        if(taken !=true)
            throw new EquipmentNotProvidedException();
        taken =false;

    }
}
