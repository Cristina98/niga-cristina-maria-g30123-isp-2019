package aut.utcluj.isp.ex2;

/**
 * @author stefan
 */
public class Equipment {
    private String name;
    private String serialNumber;

    public Equipment(String serialNumber) throws UnsupportedOperationException {
        if(serialNumber.isEmpty())
            throw new UnsupportedOperationException("Not supported yet.");
        this.serialNumber=serialNumber;
        this.name = "NONE";

    }

    public Equipment(String name, String serialNumber) throws UnsupportedOperationException {
        if(name.isEmpty()||serialNumber.isEmpty())
        throw new UnsupportedOperationException("Not supported yet.");
        this.name=name;
        this.serialNumber=serialNumber;
    }

    public String getName() {
        return name;
    }

    public String getSerialNumber() {
        return serialNumber;
    }
}
