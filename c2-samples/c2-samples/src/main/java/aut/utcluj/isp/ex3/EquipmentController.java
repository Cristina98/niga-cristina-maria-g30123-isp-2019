package aut.utcluj.isp.ex3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author stefan
 */
public class EquipmentController {

    /**
     * Add new equipment to the list of equipments
     *
     * @param equipment - equipment to be added
     */
    List<Equipment> eq=new ArrayList<Equipment>();
    public void addEquipment(final Equipment equipment) {
        //throw new UnsupportedOperationException("Not supported yet.");
        eq.add(equipment);


    }

    /**
     * Get current list of equipments
     *
     * @return list of equipments
     */
    public List<Equipment> getEquipments() {
       // throw new UnsupportedOperationException("Not supported yet.");
            for(int i=0;i<eq.size();i++) {
                eq.get(i);
                return eq;
            }
            return null;

    }

    /**
     * Get number of equipments
     *
     * @return number of equipments
     */
    public int getNumberOfEquipments() {
      //  throw new UnsupportedOperationException("Not supported yet.");
        return eq.size();
    }

    /**
     * Group equipments by owner
     *
     * @return a dictionary where the key is the owner and value is represented by list of equipments he owns
     */
    public Map<String, List<Equipment>> getEquipmentsGroupedByOwner() {
      // throw new UnsupportedOperationException("Not supported yet");
        return  null;

    }

    /**
     * Remove a particular equipment from equipments list by serial number
     * @param serialNumber - unique serial number
     * @return deleted equipment instance or null if not found
     */
    public Equipment removeEquipmentBySerialNumber(final String serialNumber) {
        for(Equipment e:eq)
        {
            for(int i=0;i<eq.size();i++)
                if(e.getSerialNumber()==serialNumber)
                {
                    eq.remove(e);
                    return e;
                }

        }
        return  null;

    }



}
