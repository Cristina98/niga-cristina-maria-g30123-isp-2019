package niga.cristina.lab10.ex3;

public class Counter extends Thread {
    String n;
    Thread t;
    int first;

    public Counter (String n, Thread t, int first) {
        this.n = n;
        this.t = t;
        this.first = first;
    }
    public void run(){
        System.out.printf (n+" a intrat in metoda run\n");
        try{
            if(t!=null)t.join ();
            System.out.println (n+" executa operatie.");
            for(int i=first;i<first+100;i++)
                System.out.println (i+1);
            Thread.sleep (2000);
            System.out.println (n+" a terminat operatia.");
        }catch (Exception e){
            e.printStackTrace ();
        }
    }

    public static void main (String[] args) {
        Counter w1 = new Counter ("Proces 1",null,0);
        Counter w2 = new Counter ("Proces 2",w1,100);
        w1.start ();
        w2.start ();
    }
}
