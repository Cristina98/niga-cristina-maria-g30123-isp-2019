package niga.cristina.lab10.ex2;

public class Punct {
    int x;
    int y;

    public Punct(){

    }
    public Punct(int x,int y)
    {
        this.x=x;
        this.y=y;
    }

    public int getY() {
        return y;
    }

    public int getX() {
        return x;
    }
}
