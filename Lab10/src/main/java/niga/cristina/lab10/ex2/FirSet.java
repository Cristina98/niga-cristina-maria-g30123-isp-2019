package niga.cristina.lab10.ex2;

import static java.lang.Thread.sleep;

public class FirSet extends  Thread{

    Punct p;

    public FirSet(Punct p){
        this.p = p;
    }

    public void run(){
        int i=0;
        int a,b;
        while(++i<15){
            synchronized(p){
                a= p.getX();
                try {
                    sleep(50);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                b = p.getY();
            }
            System.out.println("Am citit: ["+a+","+b+"]");
        }
    }
}


