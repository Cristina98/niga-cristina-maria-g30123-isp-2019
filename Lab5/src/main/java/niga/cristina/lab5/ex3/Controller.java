package niga.cristina.lab5.ex3;

public class Controller {


    TemperatureSensor a = new TemperatureSensor () {
        @Override
        public int readValue () {
            return super.readValue ();
        }
    };
    LightSensor b = new LightSensor () {
        @Override
        public int readValue () {
            return super.readValue ();
        }
    };

    public void getControl () {
        for (int i = 1; i < 21; i++) {

            System.out.println (""+i+")Sensor temperature: " + a.readValue () + ", and the lights sensor reads: " + b.readValue ());
            try {
                Thread.sleep (1000);
            } catch (InterruptedException ex) {
                ex.printStackTrace ();
            }
        }
    }
}