package niga.cristina.lab5.ex1;

public class Circle extends Shape {
    protected double radius;

    public Circle () {
        super();
        this.radius=1.0;
    }

    public Circle (double radius) {
        this.radius = radius;
    }

    public Circle (String color, boolean fillded, double radius) {
        super (color, fillded);
        this.radius = radius;
    }

    public double getRadius () {
        return radius;
    }

    public void setRadius (double radius) {
        this.radius = radius;
    }
    public double getArea(){
        return this.radius*this.radius*Math.PI;
    }
    public double getPerimeter(){
        return 2*Math.PI*this.radius;
    }

    @Override
    public String toString () {
        return "Circle{" +
                "radius=" + radius +
                ", color='" + color + '\'' +
                ", fillded=" + fillded +
                '}';
    }
}

