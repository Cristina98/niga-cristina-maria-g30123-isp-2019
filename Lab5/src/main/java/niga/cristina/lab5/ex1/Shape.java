package niga.cristina.lab5.ex1;

public abstract class Shape {
    protected String color;
    protected boolean fillded;

    public Shape () {
        this.color="Red";
        this.fillded=true;
    }

    public Shape (String color, boolean fillded) {
        this.color = color;
        this.fillded = fillded;
    }

    public String getColor () {
        return color;
    }

    public boolean isFillded () {
        return fillded;
    }

    public void setColor (String color) {
        this.color = color;
    }

    public void setFillded (boolean fillded) {
        this.fillded = fillded;
    }

    public double getArea(){
        return 0;
    }
    public double getPerimeter(){
        return 0;
    }

    @Override
    public String toString () {
        return "Shape{" +
                "color='" + color + '\'' +
                ", fillded=" + fillded +
                '}';
    }
}
