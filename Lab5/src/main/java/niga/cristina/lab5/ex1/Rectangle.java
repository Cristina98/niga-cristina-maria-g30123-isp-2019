package niga.cristina.lab5.ex1;

public class Rectangle extends Shape {
    protected double width;
    protected double length;

    public Rectangle () {
        super();
        this.width=2;
        this.length=3;
    }

    public Rectangle (double width, double length) {
        this.width = width;
        this.length = length;
    }

    public Rectangle (String color, boolean fillded, double width, double length) {
        super (color, fillded);
        this.width = width;
        this.length = length;
    }

    public double getWidth () {
        return width;
    }

    public void setWidth (double width) {
        this.width = width;
    }

    public double getLength () {
        return length;
    }

    public void setLength (double length) {
        this.length = length;
    }
    public double getArea(){
        return this.width*this.length;
    }
    public double getPerimeter(){
        return 2*(this.length+this.width);
    }
    public String toString(){
        return "Rectangle with width " +this.width + " , length "+this.length+" ,color"+this.color+", filled: "+this.fillded;
    }
}
