package niga.cristina.lab6.ex3;

import java.util.Objects;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount (String owner, double balance) {
        this.owner = owner;
        this.balance = balance;
    }


    public void withdraw (double amount) {
        if (amount < 0) {
            System.out.println ("No founds");
        } else {
            balance = balance - amount;
        }
    }

    public void deposit (double amount) {
        balance = balance + amount;
    }

    public String getOwner () {
        return owner;
    }

    public double getBalance () {
        return balance;
    }

    public void setOwner (String owner) {
        this.owner = owner;
    }

    public void setBalance (double balance) {
        this.balance = balance;
    }

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (!(o instanceof BankAccount)) return false;
        BankAccount that = (BankAccount) o;
        return Double.compare (that.balance, balance) == 0 &&
                Objects.equals (owner, that.owner);
    }

    @Override
    public int hashCode () {
        return Objects.hash (owner, balance);
    }

    @Override
    public String toString () {
        return "BankAccount{" +
                "owner='" + owner + '\'' +
                ", balance=" + balance +
                '}';
    }
}

