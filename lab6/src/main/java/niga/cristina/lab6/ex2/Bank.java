package niga.cristina.lab6.ex2;



import java.util.ArrayList;
import java.util.Comparator;

public class Bank {
    ArrayList<BankAccount> myList = new ArrayList<BankAccount> ();

    public void addAccount (String owner, double balance) {
        BankAccount bank = new BankAccount (owner,balance);
        myList.add (bank);
    }
    public void printAccounts(){
        myList.sort (Comparator.comparingDouble (BankAccount::getBalance));
        for(BankAccount i : myList)
            System.out.println (i.getOwner ()+ " "+i.getBalance ());
    }
    public void printAccounts(double minRange,double maxRange){
        for(BankAccount i :myList)
            if(i.getBalance ()>= minRange && i.getBalance () <= maxRange)
                System.out.println (i.getOwner () + " "+i.getBalance ());
    }
    public  ArrayList<BankAccount> getAllAccounts(){
        myList.sort (Comparator.comparing (BankAccount::getOwner));
        return myList;
    }
}
