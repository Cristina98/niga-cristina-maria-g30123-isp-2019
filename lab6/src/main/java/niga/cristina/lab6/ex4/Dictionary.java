package niga.cristina.lab6.ex4;
import java.util.HashMap;
import java.util.Scanner;
public class Dictionary {
    private HashMap<Word, Definition> dictionary = new HashMap<Word, Definition> ();

    void addWord (Word w, Definition d) {
        dictionary.put (w, d);
    }

    Definition getDefinition (Word w) {
        if (dictionary.containsKey (w)) {
            System.out.println ("" + dictionary.get (w).getDescription ());
            return dictionary.get (w);
        }
        System.out.println ("Cuvant negasit");
        return null;
    }

    public void getAllWords () {
        for (Word w : dictionary.keySet ()) {
            System.out.println ("" + w.getName ());
        }
    }

    public void getAllDefinitons () {
        for (Definition d : dictionary.values ()) {
            System.out.println ("" + d.getDescription ());
        }
    }

    public void readWordandDefinition () {
        String cuvant;
        String def;
        Scanner citire = new Scanner (System.in);
        System.out.println ("Dati cuvantul pe care doriti sa l adaugati:");
        cuvant = citire.nextLine ();
        System.out.println ("Cuvantul dat este: " + cuvant);
        System.out.println ("Dati o definitie pentru acest cuvant: ");
        def = citire.nextLine ();
        Word myWord = new Word ();
        myWord.setName (cuvant);
        Definition myDefinition = new Definition ();
        myDefinition.setDescription (def);
        addWord (myWord, myDefinition);
    }

    public void readWord () {
        boolean valid = false;
        String cuvant;
        System.out.println ("Pentru ce cuvant vreti definitia: ");
        Scanner citire = new Scanner(System.in);
        cuvant = citire.nextLine ();
        Word myWord = new Word ();
        myWord.setName (cuvant);
        for (Word word2 : dictionary.keySet ())
            if (word2.getName ().equals (myWord.getName ())) {
                System.out.println ("Definitia este: " + dictionary.get (word2).getDescription ());
                valid = true;
            }
        if (!valid) System.out.println ("Cuvantul introdus nu exista!");
    }
}
