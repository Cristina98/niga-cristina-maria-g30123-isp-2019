import niga.cristina.lab6.ex1.BankAccount;

public class ex1Test {
    public static void main(String[] args)
    {
        BankAccount bankAccount1=new BankAccount("Cristi" ,1500);
        BankAccount bankAccount2=new BankAccount("Alex", 2000);
        bankAccount1.deposit(500);
        bankAccount2.withdraw(20);
        System.out.println(bankAccount1.toString());
        System.out.println(bankAccount2.toString());
        if(bankAccount1.hashCode()==bankAccount2.hashCode())
        {
            if(bankAccount1.equals(bankAccount2))
            {
                System.out.println("Au aceeasi suma");

            }
            else
                System.out.println(("Alex are o suma mai mare"));
        }
        else
            System.out.println("Hash diferit");

    }
}
