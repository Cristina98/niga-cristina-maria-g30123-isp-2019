package niga.cristina.lab7.ex1;

public class CoffeTest {

    public static void main (String[] args) {
        CoffeMaker mk = new CoffeMaker ();
        CoffeDrinker d = new CoffeDrinker ();

        for (int i = 0; i < 15; i++) {
            Coffe c = mk.makeCoffe ();
            try {
                d.drinkCoffe (c);
            } catch (TemperatureException e) {
                System.out.println ("Exception:" + e.getMessage () + " temp=" + e.getTemp ());
            } catch (ConcentrationException e) {
                System.out.println ("Exception:" + e.getMessage () + " conc=" + e.getConc ());

            } catch (NumberException e) {
                System.out.println ("Exception:" + e.getMessage () + " num=" + e.getNr ());
                break;
            } finally {
                System.out.println ("Throw the coffe cup.\n");


            }
        }
    }
}//.class

class CoffeMaker {
    Coffe makeCoffe () {
        System.out.println ("Make a coffe");
        int t = (int) (Math.random () * 100);
        int c = (int) (Math.random () * 100);

        Coffe coffe = new Coffe (t, c);
        return coffe;
    }

}//.class

class Coffe {
    private int temp;
    private int conc;
    public static int nr = 0;


    Coffe (int t, int c) {
        temp = t;
        conc = c;
        nr++;
    }

    int getTemp () {
        return temp;
    }

    int getConc () {
        return conc;
    }

    int getNr () {
        return nr;
    }

    public String toString () {
        return "[coffe temperature=" + temp + ":concentration=" + conc + "numar=" + nr + "]";
    }
}//.class

class CoffeDrinker {
    void drinkCoffe (Coffe c) throws TemperatureException, ConcentrationException, NumberException {
        if (c.getNr () > 9)
            throw new NumberException (c.getNr (), "Coffe number to high!");
        if (c.getTemp () > 60)
            throw new TemperatureException (c.getTemp (), "Coffe is to hot!");
        if (c.getConc () > 50)
            throw new ConcentrationException (c.getConc (), "Coffe concentration to high!");

        System.out.println ("Drink coffe:" + c);
    }
}//.class

class TemperatureException extends Exception {
    int t;

    public TemperatureException (int t, String msg) {
        super (msg);
        this.t = t;
    }

    int getTemp () {
        return t;
    }
}//.class

class ConcentrationException extends Exception {
    int c;

    public ConcentrationException (int c, String msg) {
        super (msg);
        this.c = c;
    }

    int getConc () {
        return c;
    }
}//.class

class NumberException extends Exception {
    int n;

    public NumberException (int nr, String msg) {
        super (msg);
        this.n = nr;
    }

    int getNr () {
        return n;
    }
}//.class