package ro.utcluj.aut.isp.vehicles;

public class Vehicle {

    private String type;
    private int weight;
    private int length;


    public Vehicle(String type, int length,int weight) {
        this.type = type;
        this.length=length;
        this.weight=weight;
    }
    public Vehicle(String type, int length) {
        this.type = type;
        this.length=length;
    }

    public int getLength() {
        return length;
    }

    public int getWeight() {
        return weight;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vehicle vehicle = (Vehicle) o;
        return length == vehicle.length &&
                type.equals(vehicle.type)
                && weight==(vehicle.weight);
    }
    public String start(){
        return "engine started";
    }

}
