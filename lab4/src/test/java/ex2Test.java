import niga.cristina.lab4.ex2.Author;
import org.junit.Test;
import org.junit.Assert;

import static org.junit.Assert.assertEquals;
public class ex2Test {
    @Test
    public void testAuthor(){
        Author myAuthor = new Author ("Cristina","cristinaniga98@yahoo.es",'f');
        assertEquals (" Cristina(f) at cristinaniga98@yahoo.es",myAuthor.toString ());
    }
}