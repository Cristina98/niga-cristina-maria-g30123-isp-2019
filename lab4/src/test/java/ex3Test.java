import niga.cristina.lab4.ex2.Author;
import niga.cristina.lab4.ex3.Book;
import org.junit.Test;
import org.junit.Assert;

import static org.junit.Assert.assertEquals;

public class ex3Test {
    @Test
    public void test(){
        Author myAuthor = new Author ("Cristina","cristinaniga98@yahoo.es",'f');
        Book myBook= new Book ("Carte1",myAuthor,35.5);
        assertEquals ("Book-Carte1 by  Cristina(f) at cristinaniga98@yahoo.es",myBook.toString ());
    }
}