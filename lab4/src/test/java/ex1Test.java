import niga.cristina.lab4.ex1.Circle;
import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class ex1Test {
    @Test
    public void testCircle () {
        Circle myCircle = new Circle ();
        Circle myCircle2 = new Circle (2);
        assertEquals (1,myCircle.getRadius (),1.0);
        assertEquals (2, myCircle2.getRadius (), 1.0);
        assertEquals (Math.PI*Math.pow (2,2),myCircle2.getArea (),1.0);

    }
}
