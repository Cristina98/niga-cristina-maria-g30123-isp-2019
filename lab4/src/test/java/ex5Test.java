import niga.cristina.lab4.ex5.Circle;
import niga.cristina.lab4.ex5.Cylinder;
import org.junit.Test;
import org.junit.Assert;

import static org.junit.Assert.assertEquals;

public class ex5Test {
    @Test
    public void test () {
        Circle myCircle = new Circle (2.0);
        Circle myCircle2 = new Circle ();
        assertEquals (2.0, myCircle.getRadius (), 1.0);
        assertEquals (1.0, myCircle2.getRadius (), 1.0);
        assertEquals (Math.PI * Math.pow (2, 2), myCircle.getArea (), 1.0);
        Cylinder myCylinder = new Cylinder (1);
        assertEquals (1, myCylinder.getHeight (), 1.0);
        assertEquals (Math.PI * Math.pow (1, 1) * 0, myCylinder.getVolume (), 1.0);

    }
}
