import niga.cristina.lab4.ex5.Circle;
import niga.cristina.lab4.ex6.Square;
import org.junit.Test;
import org.junit.Assert;

import static org.junit.Assert.assertEquals;

public class ex6Test {
    @Test
    public void test(){
        Circle myCircle = new Circle(1);
        Circle myCircle2= new Circle ();
        Square mySquare= new Square ();
        assertEquals (2.0, myCircle.getRadius (), 1.0);
        assertEquals (1.0, myCircle2.getRadius (), 1.0);
        assertEquals (Math.PI*Math.pow (1,1),myCircle.getArea (),1.0);
        assertEquals ("A square with side = 1.0 which is a subclass of A rectangle with width= 1.0 and length = 1.0 which is a subclass of A Shape with color of green and true",mySquare.toString ());

    }
}
