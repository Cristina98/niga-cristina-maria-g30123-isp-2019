package niga.cristina.lab2.ex3;

import java.util.Scanner;

public class Main {

    private static boolean checkIfPrime(int x)
    {
        boolean OK = true;
        for(int i=2;i<=Math.sqrt(x);i++)
            if (x%i == 0) OK = false;

        return OK;
    }

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduceti un interval: ");

        int A = scanner.nextInt();
        int B = scanner.nextInt();
        int numar=0;

        if(A<2) A=2;

        for(int i = A ; i <= B; i++)
            if(checkIfPrime(i)) {System.out.println(i + " ");numar++;}

        System.out.println("Numarul de numere prime este: " + numar);

    }
}

