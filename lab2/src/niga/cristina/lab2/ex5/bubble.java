package niga.cristina.lab2.ex5;


public class bubble {
    public void bubbleSort (int sir[]) {
        int n = sir.length;
        int temp;
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (sir[j] > sir[j + 1]) {

                    temp = sir[j];
                    sir[j] = sir[j + 1];
                    sir[j + 1] = temp;
                }
    }

    public void afisare (int sir[]) {
        int n = sir.length;
        System.out.println ("Vector sortat:");
        for (int i = 0; i < n; i++) {
            System.out.println (sir[i] + " ");

        }
    }
}
