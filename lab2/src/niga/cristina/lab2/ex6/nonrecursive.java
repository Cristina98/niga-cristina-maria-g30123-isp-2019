package niga.cristina.lab2.ex6;

import java.util.Scanner;

public class nonrecursive {
    public void factorial(){
        int i;
        int factorial=1;
        System.out.println ("n!=");
        Scanner citire=new Scanner (System.in);
        int n=citire.nextInt ();
        if(n==1){
            System.out.println ("Factorial:1");
        }
        else{
            for(i=1;i<=n;i++){
                factorial*=i;
            }
            System.out.println ("Factorial="+factorial);
        }
    }
}
