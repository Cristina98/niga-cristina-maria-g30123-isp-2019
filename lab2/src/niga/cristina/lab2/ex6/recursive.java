package niga.cristina.lab2.ex6;

import java.util.Scanner;

public class recursive {
    public int factorialRecursiv(int N)
    {

        if(N > 1)
            return N * factorialRecursiv(N - 1);
        else
            return 1;
    }
}
